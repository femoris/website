---
title: 'AirTabla: A theremin inspired tabla'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - mentor
        - audioInterface
        - kids
        - playground
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: ''

---

Mentoring students of standard 7-10th during an intel sponsored hackathon.

===
### About
As a part of Intel Ideathon, this idea was implemented by 4 kids of age 16-18 years. Inspired from the theramin, using Intel Galileo, kids programmed generation of sound of different frequencies mapped to hand movement. This was taken further and displayed the system at NASSCOM – 2015 and permanent artifact at Tech Museum @ Rashtrapati Bhavan, Delhi
![](schematic.jpg)
![](president_with_my_work.jpg)