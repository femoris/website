---
title: 'Pulse: Crowd sensing for smarter cities'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - interaction design
        - HCI4D
        - visualisation
        - data collection methods
        - crowd sensing
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: ''

download:
  url: pulse_lolX.pdf
  alt: Presentation
---

A method to collect urban data through crowd-sensing in smart cities

===

## Overview
The advent of smart cities has increased the demand for agile and effective governance. This paper presents a prototype at the proof of concept stage, called “Pulse”. The system comprises of – a mobile app for citizens to submit feedback, a web dashboard for district administrators and government bodies to push close-ended questions based on locality, gender, age group and time, and a network of [eddystone beacons](https://en.wikipedia.org/wiki/Bluetooth_low_energy_beacon) that pushes close-ended questions about urban spaces, creating a hyperlocal crowdsensing framework in the Indian context. The collected data can be later analysed to generate meaningful insights and visualizations for better governance and smart decision/policy making.
An integral part of this project was social engineering. How the contextual questions can be framed for any city in general. We used the framework suggested by Benouaret, et al in their research on using social engineering to sense crowd called CrowdSC [Building Smart Cities with Large-Scale Citizen Participation. IEEE internet computing 17, no. 6 (2013)]

A detailed documentation can be found in the form of a paper here.

A video explaining the usage scenario is found over [here](https://www.youtube.com/watch?v=4CF1xL61bsY), a special thanks to Raagul M (he’s the best around me when it comes to camera)

This project *won* the i_hack hackathon by facebook in January 2017 ([presentation](pulse_lolX.pdf)) and also received critical feedback from various reviewers of international conferences. This encouraged me to write a paper and document it academically, although the evaluation and user studies part was not up to the mark. The paper can be found [here](https://drive.google.com/file/d/1DZaHUva56XCM-47UcNNMaRZttUyN2Bi4/view)

This project was conceived during the course on Interaction, Media and Senses by Prof Ravi Poovaiah and Dr. Ajanta Sen.



