---
title: 'Epidemic Informatics: Real time visualisation of epidemics'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - interaction design
        - HCI4D
        - visualisation
        - data collection methods
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: ''

download:
  url: epidInfo.pdf
  alt: Presentation
---


A system for health department of BMC to generate real time information visualisation of epidemic spread.

===

## Overview
Preventing epidemic from occurring and spreading is the one of the major motives of any government health ward. Collecting information about the epidemics is done via manual reporting in the current state. Information about epidemics require faster ways of reporting to take actions and prevent it from spreading. We leveraged the increased usage of smartphone to increase a faster reporting of epidemics in a city.

We conducted user studies to understand the existing flow of information between entities ranging from the epidemic cell to local foot soldiers over a period of two weeks to design and develop an application that will allow faster reporting with very less cognitive load on the foot soldiers.

Presentation can be viewed [here](epidInfo.pdf).

