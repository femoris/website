---
title: 'Prof. X: BCI enabled home automation'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - BCI
        - tinkering
        - playground
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: ''

download:
  url: bci_paper.pdf
  alt: Paper
---

Imagine if you had the powers of Professor X (X-men comics) ! You were able to control things and 
machines jusssssst with your thought !

===

### Tinkering

Brain Computer Interfaces allows us to detect brain signals and isolate them when performing specific tasks. For e.g: What kind of signals are generated while thirsty ? or in need for fan or while being distracted ?

We thought of operating home automation systems with brain signals. “Think of the light and the light turns on”

“Think of fan and the fan turns on”

[plugin:youtube](https://www.youtube.com/watch?v=cBELgnuMtqU)
A sample video, where a bald me tries to lift the curtain using my thoughts.

More videos….
[plugin:youtube](https://www.youtube.com/watch?v=jTA-U1nJq3Q)
[plugin:youtube](https://www.youtube.com/watch?v=r-BsSMGwI3Y)
[plugin:youtube](https://www.youtube.com/watch?v=jXnWpK8ZC5Y)
[plugin:youtube](https://www.youtube.com/watch?v=srlIf-f3CBE)

We published a short paper at ICMRP, Ahmedabad in 2014. An excerpt is hosted [here](https://drive.google.com/file/d/1wJMDOdwcNSv3re4gWR0yti4k1o-YKGV8/view)
