---
title: 'User study: Technological opportunities to support kirana stores in the age of capitalist consumerism'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - user study
        - ethnography
        - case study
        - course work
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: ''

download:
  url: kirana_dapoli_.pdf
  alt: Paper (Please email for key)
---

A 10 day ethnographic study of kirana stores with focus on micro-lending in India

===

## Overview
While modern trade has been growing in India over the last 25 years, small grocery stores (also known as kirana shops) continue to have over 90% of the market share. An interesting practice in these stores is that they provide credit to their customers, which is a major draw for some of their loyal customers. However, this practice often leads to a crunch of working capital for the shopkeeper. Could these traditional credit practices be better supported with use of technology? In August 2016, we conducted contextual inquiries with 103 users (including 61 shopkeepers and 42 customers) across a metro city, a small town and 4 villages. Our methodology included store visits, observations, contextual interviews with shopkeepers and customers and account-book studies. Our objective was to understand the drivers of such credit practices in kirana stores. We found that the shopkeepers have developed interesting strategies to maximize their business and to mitigate their risks. These practices and strategies varied based on the location and the type of custom that a shop entertained. We found that kirana stores were an essential part of the community. The shopkeepers feel the need to retain customers and to establish social goodwill by giving credit. Trust plays a vital role in the shopkeepers’ decision making process. Using these findings, we propose a “Trust-Credit model” that can guide future design interventions. Coincidentally, on November 8, 2016, the Government of India decided to demonetize the ₹500 and the ₹1,000 currency notes. This took away about 86% of the available currency in a cash-dependent market overnight and triggered sudden and quick changes in buying and credit behaviour. To understand this better, we conducted addition 16 interviews in Mumbai. We find that our model still holds up, but the new scenario acts as a trigger for new behaviours.

Presentation about insights can be found over [here](https://drive.google.com/file/d/1quMBkT9AT2cbzIkfJmU_1YPeE7IjxNDV/view). 

Since the paper is under review at a conference, only abstract is described. Please mail to malay.n.dhamelia@gmail.com for receiving user's key after downloading the [pdf](kirana_dapoli_.pdf), if need arises. 