---
title: 'Digitable: An android software to detect a fruit'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - mentor
        - howTo
        - kids
        - playground
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: ''

published: false

---

A how to for kids, and thoughts on 3D object recognition in AR using Vuforia and Unity. The project was developed with a kid and it is for kids who wish to get their hands on software engineering and getting used to some technology and create interesting things using their own imagination. To imagine, is to live. Hop on. 

===
### My first attempt in alternative realities
Coming across alternative realities like Augmented reality (AR), Virtual reality (VR), and Mixed reality (MR) is an un-asked gift you get when you study in a school like IDC. Folks there were fascinated by the possibilities offered by such domains. Trippy as they sound, alternative realities were never really my interest, and I have always looked them with a skeptic's eye. I could see the possibilities, opportunities offered by the domain, but never tried them. I always thought about them as naive playtoys, or at the max a collective technological dream. It was in a way good to have, but not necessary to have. Even if they were used with some purpose, I always believed that they are too much for too less. 

A cousin, who studies in 9th class, asked for help with her science fair project. She wanted somehow to display information about fruit nutrition. She believed that this was a necessary feat of science--- to help people know about their nutrition intake, and hence stay healthy. Cool. A good opportunity to try AR with her and prototype her dream. The post is written for kids to understand the technology of AR and how to make such an application using an android smartphone on their own. A good weekend project to play with. 

### Goal of the project
By the end of this project, you will make a super cool android application that will identify one particular fruit and show its nutrition values. You can also try the same application with various other objects to display their properties. Meanwhile you will also learn about what makes a banana :P

### What all you need, and how to get it
You will require:
1. A computer (2GB of RAM is sufficient. More is better in this case) with Windows installed. This tutorial only covers computers with windows OS, for linux systems, email me.
2. An android smartphone (with atleast 1GB of RAM. More is better, but here, the most important configuration is of camera. It should be no less than 8 mega pixels (or 8 MP))
3. A micro USB cable (the one that you use to charge your phone).

Once you have these things, assure yourself that you are good to go :+1:
You will also need some softwares. Now, these are not out-of-box softwares that come with your standard windows OS installation. You will need to download these from various websites. Next section covers that. 

### All the softwares you will need
After this section, you will have all the softwares you need. They are listed for reference:
1. Unity3D
3. Java Development Kit
2. Android Studio.

Once these softwares are setup, next section gets you going towards making the actual application. If you have the environment ready for development, you may want to skip through the next section. 

##### Unity
You will need unity. Unity is a software that is used to develop games. Real fun games. It is very easy to learn, and it is free ! Very few good softwares are free (ask why ?). To get unity on your computer, visit unity downloads website by clicking [here](https://unity3d.com/get-unity/download). Follow the screen shots. 

![](1.png)
![](2.png)

Once you have downloaded the exe (short for executable file), install it on your computer. These have standard installation procedures. This completes installation of unity.

##### Installing JDK
We need to install Java development kit since, android runs on java and java based wrappers like gradle. To do that visit [this website](http://www.oracle.com/technetwork/java/javase/downloads/index.html) and follow the instruction shots below:
![](images/jdk_1.jpg)
![](images/jdk_2.jpg)
The .exe file needs to be installed with a standard procedure. This ends installing JDK

##### Android SDK
Installing and integrating Android SDK standalone can be tricky. To be on the safe side, let us install android studio complete. Visit [android studio website](https://developer.android.com/studio/) and follow the instruction shots below:
![](images/android_studio_0.jpg)
![](images/android_studio_1.jpg)

Now that you have downloaded unity and android studio, we have to integrate them. This way we ask unity to build using android SDK. To do that, perform following steps:

![](images/new/android_config_1.jpg)
![](images/new/android_config_2.png)

Address for Android SDK is usually C:\Users\YOUR_USER_NAME\AppData\Local\Android\sdk and for JDK, it should look something like this C:\Program Files\Java\jdk1.8.0_66 (It is alright if you have other versions than 1.8.0_66, but keep in mind, this project works best in jdk1.7 or more.)

##### Vuforia integration in unity
Open unity from start menu. The welcome screen of unity looks like below. Create a new project and save it whereever you like. Make sure, you select 3D. 
![](images/unity_1.jpg)
![](images/unity_2.jpg)

###### A brief intro to unity intreface
The red section in the image is called _hierarchy_ where you create scenes and drop whatever you want to put (cubes, text, spheres, human models, etc). Elements of hierarchy are reflected in the _scene section_ which is of yellow color. Each element has certain properties; for example, a cube has height, width and length, color, texture, position in space. These properties are displayed and can be manipulated in the _inspector section_ in blue. Lastly, the green colored section is your repository or a store house of projects things. 
![](images/unity_intro.jpg)

Continuing the installation and integration of vuforia in unity, follow the instruction shot. At this point, make sure you have a unity account. If not, you can create it using [this link](https://id.unity.com/en). Note that this is a UNITY account, NOT VUFORIA account. Both of them are separate and different. All the registration procedures are standard and easy. 

![](images/unity_intro_asset.jpg)
![](images/unity_intro_login.jpg)
OR, login using
![](images/unity_intro_login_alt.jpg)
This will make the unity asset store active and you'll see a screen something like this. 
![](images/unity_intro_asset_active.jpg)
![](images/unity_vuforia.jpg)
![](images/unity_vuforia_import.jpg)
![](images/unity_vuforia_success.jpg)

By this time vuforia is installed correctly. Now you will need to activate it. To do this, perform following steps:
![](images/unity_activate_vuforia.jpg)
![](images/unity_activate_vuforia_2.jpg)
![](images/unity_activate_vuforia_3.jpg)

The computer is now ready for development. You can take a break, stretch a leg, drink chocolate milk (this will refill the drained glucose due to all this headbanging.)

### Scanning the object
Now, we need to scan the object. To do that, we will need to download vuforia's application on your smartphone. Visit [this website](https://developer.vuforia.com/downloads/tool) and follow the instruction shots to make your application up and running:

![](images/scann_app.jpg)
![](images/scann_app_2.jpg)
![](images/scann_app_3.jpg)
![](images/scann_app_4.jpg)
![](images/scann_app_5.jpg)
![](images/scann_app_6.jpg)
![](images/scann_app_7.jpg)
![](images/scann_app_8.jpg)
![](images/scann_app_9.jpg)

Once you have scanned the object your choice, you will need to upload it to the vuforia website to create a database out of it. To do this, we will need to generate keys. Following procedure will get you keys. 

![](images/keys.jpg)
![](images/keys_2.jpg)
![](images/keys_.jpg)
![](images/keys_4.jpg)
![](images/keys_5.jpg)
![](images/keys_6.jpg)

Once you have your keys, its time to scan and upload your .od file. Open your vuforia account and go to target manager via the [developer portal of vuforia](https://developer.vuforia.com/home-page). In the developer portal mentioned about, after logging in, find Develop > Target Manager. Follow these steps to add your object and set it as a target. 
![](images/unity_7_.jpg)
![](images/add_target.jpg)
![](images/add_target_2.jpg)
![](images/add_target_3.jpg)
![](images/add_target_4.jpg)

Once you have got the keys, download the unity package I have prepared for you from [here](https://drive.google.com/open?id=1jIYt9377YW-Ul-1qS3BvS_PkRkV0Wira). You can now import the package by following through these instruction shots.

![](images/import/1.jpg)
![](images/import/2.jpg)
![](images/import/3.jpg)
![](images/import/4.jpg)
![](images/import/5.jpg)
![](images/import/6.jpg)
![](images/import/7.jpg)
![](images/import/7a.jpg)
![](images/import/7b.jpg)
![](images/import/7c.jpg)
![](images/import/7d.jpg)
![](images/unity_8_.jpg)
Select YOUR database name and target object as shown. 
![](images/unity_10_.jpg)
![](images/import/8.jpg)
![](images/import/9.jpg)
![](images/import/10.jpg)
For adding more objects that are scanned by you, duplicate the object by following steps. Dont forget to change the database and the target in inspector. Note that in order to  
![](images/import/11.jpg)

### Build an APK
Now that everything is in place, its time to build and bake the android package (also called apk). Follow through these screen shots to build. 
![](images/import/build_1.jpg)
![](images/import/build_3.jpg)
![](images/import/build_4.jpg)
![](images/import/end.jpg)
![](images/import/end_2.jpg)

Cheers ! 
Have a fun weekend!

### Thoughts 
##### On Object recognition with vegetation. 
A list of challenges if to be used with Vuforia object recognition or which vegetables work and why. 
+ High contrast vegetation is desirable. Fruits, and vegetables with high contrast, like bananas, papayas, pixie watermelons. They have a high contrast in their patters. *Black patterns* of ripening on *yellow peel* in *bananas* and *papayas*, striped patterns of *light and dark green* on *pixie watermelons*. 
+ Choose a fruit that does not ripen quickly. Bananas ripe quickly and hence the patterns on the skin change. This baffles Vuforia since this is not something that it is intended to do. But, I think it is cool for a 9th grade kid to find this and repurpose it. 

Always feels good to help an inquisitive person. Especially feels good, when they grow their ideas. Your efforts feel satisfied. 

