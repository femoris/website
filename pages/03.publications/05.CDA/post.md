---
title: 'Cultural Domain Analysis for Soundscape Assessment'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - paper
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: ''
download:
	url: CDA_paper.pdf
	alt: Paper (Please email for key)
	
---

Dhamelia M, Dalvi G. (2018). Cultural Domain Analysis for Soundscape Assessment, ICorD 2018, Bangalore

===

## Abstract
Sonic environment consisting of various sounds ranging from music and natural sounds to artificial sounds is an integral part of any geography. It is a reflection of people living in the location as well as an influencer of culture in that location. To assess the perception of a person or people of context offered by that location, a soundscape assessment is performed that majorly involve likert based questions for pleasantness, annoyance, appropriateness and so forth. 

We propose a qualitative method to assess soundscapes from a cultural point of view derived and tweaked from existing methods and tools like the Swedish soundscape quality protocol(SSQP). The method stands as a counterpart of qualitative soundscape assessment approaches like the SSQP, however, it provides richer insights into the perception of the soundscape with its effects, sound characteristics and cultural meanings. 

The developed method is also deployed to a field study in Phulenagar, an urban slum of Mumbai. Regions such as these, occupied by people of low socio-economic stratum have a rich sonic interactions because of small residences and proximity of residences. They also offer rich soundscape which is created and affected by sounds generated from various cultural, social, and economic sources and activities. The analysed results are presented in the form of insights. 