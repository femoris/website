---
title: 'Gratitude'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - 
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true

published: true
---

A man rarely laughs alone!

===

![](gratitude.jpg)
The strands of expressions came in from various unexpected places. They convoluted. It was painful at first. Only later did I realise that it was an itch of growth. Thanks Killer Queen, 5Min, and the roommates!

A sane man, rarely laughs alone. 

_This was written after using [themostdangerouswritingapp](http://themostdangerouswritingapp.com/)_