---
title: 'Define interesting'

published: true
---
> What do people actually mean when they say the word *interesting* ? Or what are interesting objects ? 


Understanding the idea of interesting is tickly. Any thing or thought, is interesting if either it is new, if it is unconventional, if it is out of place, if it is out of context. Adam Grant [faces off](https://panoply.fm/podcasts/revisionisthistory/episodes/1WBmvvvuFSGwQYoAoYqO0a) with Malcolm Gladwell on what makes an idea interesting. 

*Whatever breaks weakly held assumptions, is interesting to people*. To have interesting ideas is to understand existing assumptions, shake them and break them. Assumptions are majorly created by heuristics and biases. Locate them, understand them, and place a 'what if'; you have something mildly interesting. 