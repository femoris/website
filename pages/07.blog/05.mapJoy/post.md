---
title: 'Joy of mapping'
headline: ''
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
published: false
publish_date: '10-08-2017 22:06'
taxonomy:
    category:
        - blog
    tag:
        - 'data visualisations'
        - thoughts
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

>Travelling back to April 2017, when I made a prototype application that will aid in mapping the epidemic in realtime. First experience with any mapping business. Travel to present. Data Visualisation course. First Assignment (1 day excercise). Map anything of our insti. The students had to collect mutually exculsive dimensions and their measures. Then, creating a ‘mashup’ of data collected and see if any data *story comes* up. I chose wireless because of several reasons. First, it is measurable. Second, it is versatile in application and a dependency for many tasks. Third, once the visualisation is done, it will have some utilitarian value associated to it.

Human beings are consumers. Of all things, first one being information. Do they love to generate information, I have a hypothesis.

Humans express. Expression can be treated as combination of two data sets in sync. Correlation between two data sets makes information. Expression is sharing of information, only made clear. Hence, we generate information. Hence, humans love to generate information as well (as consuming it).

>Thought: Can disemination of information, be the source of happiness ?

Physical space, filled with information of sorts— viewable, imaginable, percievable, abstracted, so forth. Mapping facilitates collecting that information in a systematic manner and presenting in a viewable form. This generated map, used by other people is an expression of information percieved, interpreted and represented by you.

Nuff of blab, check out my [visualisation](http://dee-am.in/live/1.html) of router locations, their strengths inside IIT B Campus. It is made using leaflet.js, leaflet.heat. 

I would like to thank Mr. Waghmare of Computer Centre for providing me the list of routers inside insti. While my brains fried with some jsfoo, Tuhin helped me debug the parser.