---
title: 'Conferences'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - list
        - resource
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: ''

publish_date: 10/1/2017 8:30 pm
published: true
---

A resource for listing various conferences or conference aggregators.

===
+ [WikiCFP](http://www.wikicfp.com/cfp/call?conference=HCI)
+ [Core](http://portal.core.edu.au/conf-ranks/)

Design journal
+ [An International Journal for All Aspects of Design](https://www.tandfonline.com/toc/rfdj20/current)


Lists are dynamic. More shall be added. Have something in mind, that you like ? Send an [email](mailto:malaydhameliareads@gmail.com)