---
title: 'Digital Magazines'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - list
        - resource
        - articles
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: ''

publish_date: 7/30/2018 11:05 pm
published: true
---

Sources for daily reads. Of ideas, of things. This list is primarily for reads that are longform, a bit academic, pioneer information disseminators of their domains, with interesting formats. 

===

+ [Aeon](https://aeon.co/)
+ [Edge](https://www.edge.org/)
+ [Nautilus](http://nautil.us/)
+ [Kialo](https://www.kialo.com/)
+ [The Dissolve](http://thedissolve.com/)
+ [The Atlantic](https://www.theatlantic.com)
+ [Vice](https://www.vice.com/en_us)
+ [Arts and Letters daily](artsandlettersdaily.com). In case domain fails to redirect, use [this](https://www.aldaily.com/).
+ [Highline](https://highline.huffingtonpost.com/)
+ [The New Yorker](https://www.newyorker.com/)
+ [If everyone knew](http://www.ifeveryoneknew.com). Exhaustible.
+ [Conversations](http://www.conversations.org/)
+ [Berfrois](https://www.berfrois.com/)
+ [Letters of note](http://www.lettersofnote.com/)
+ [The new inquiry](https://thenewinquiry.com/)
+ [The longform](https://longform.org)
+ [The Browser](https://thebrowser.com)
+ [Propeller](http://www.propellermag.com/)
+ [Undark](https://undark.org/)
+ [Harper's magazine](https://harpers.org/)
+ [Grist](https://grist.org/)
+ [readWrite](https://readwrite.com/)
+ [Paypervids](https://www.paypervids.com/)

Lists are dynamic. Think you have interesting aggregators/articles or the sources ? Send an [email](mailto:malaydhameliareads@gmail.com)