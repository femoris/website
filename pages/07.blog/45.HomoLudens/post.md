---
title: 'Homo Ludens -- A study of play-element in culture'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - procedure
        - work
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: ''

published: true
---

Part of the presentation I gave at the ACM SIGCHI-Mumbai Chapter. 

===

Recently, on September 28, 2019, I was invited to present Homo Ludens as an invited scholar at the SIGCHI-Mumbai Chapter. This provocative presentation was requested by a few people from the audience. To them, and to those, who wish to get a gist of what this dense literature is about, I have made this presentation public. It can be found [here](https://docs.google.com/presentation/d/1pK6Lz4A5cyhxRpS_Xld4SM5GfMwhRpz4G5StPqSLKQk/edit?usp=sharing)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT2PuwIwAtG9BndMPAMjJySS-yHmVxlPU1YrSY1_PmkCZqjYUFc-ypBEHw1IvMVYV5wdYAUcIEe7-3R/embed?start=false&loop=true&delayms=60000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>