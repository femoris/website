---
title: 'A list of interesting blogs'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - list
        - resource
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: ''

publish_date: 10/1/2017 8:30 pm
published: true
---

Blogging as a computer mediated communication is at times, passive. The sociology of blogging is interesting. Through [this](https://academic.oup.com/jcmc/article/12/4/1409/4583037) research I was able to understand a rough framework about blogging, its intrinsic and extrinsic motivations associated and the influence of blogging on a person. Anyhoo, here are some blogs that are/were highly active and diverse in nature, in addition to them being my favourites.  

===

+ [Hatnote](http://blog.hatnote.com/) | A blog dedicated to wiki life. A perennial source of information about WikiMediaFoundation. A very cool repository of wiki realised projects.
+ [Hyperbole and a half](http://hyperboleandahalf.blogspot.in/)
+ [Gates Notes](https://www.gatesnotes.com/)

Lists are dynamic. Have something in mind, that you like ? Send an [email](mailto:malaydhameliareads@gmail.com)


