---
title: 'Books on design, of design'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - list
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: false

published: true
---

A list of books that are suggested and curated by my friends. I hope to come out with such a list soonish as mentioned in the [post](http://www.dee-am.in/site/blog/books). 

===

I love lists. Hence, I have friends who love lists. Rohit came up with a list of books that had influenced him during the design course. He also segregates his influences semester-wise, and details out why that book makes sense in that time period. Check out that list [here](http://rohitg.in/2019/03/31/BookRecs1/). 


![](Capture.PNG)
Tuhin once stumpled upon a tweet by Dan Saffer asking for non-design books that are design books. [See the original tweet here](https://twitter.com/odannyboy/status/1115653460752359424). He has scrapped the replies to a list for us. Find it [here](https://nudb.netlify.com/) 

Hope this fills your bucket !




