---
title: 'Slow down, dont be scared'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - Zone
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true

published: false

---

Interesting sub reddit

===

[Weekly Curated Art Reddit Exhibits](https://www.reddit.com/r/Exhibit_Art/). 

> ... this is a deliberately slow-paced subreddit. It will not leap out at you from amidst the ocean of content gushing past your front page to smother you with affection.
>-subReddit Mod

Think like Sartre writes. I like slow things. 

_Read_: La Nausea
_Music_: Yann Tiersen - Comptine d'un autre ete
