---
title: 'Nepal with an interesting pal'
headline: 'It works if you say it 8P'
media_order: 'monasteries.jpg,Untitled-3.ai,planv.jpg'
published: true
taxonomy:
    category:
        - blog
    tag:
        - travel
        - memory
        - music
        - travelHacks
        - ideas
        - TravelResources
        - nepal
        - interactive travelogue
        
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

Mostly a record of moments, some hacks, some resources, and some must-do things in Nepal. Tooltips in this blogs are like my tiny mental notes. If I were you, I would not ignore the tooltips of this blog. 

===

I am aware most people are short of time in hearing out travel experiences, forget about reading them. The blog does not intend to recommend places, but provide various info that I did not find on the web before visiting Nepal. Hence, I have structured the blog as following:
1. A bit of context, what kind of travel do I prefer and such contextual info. This can help you choose if things and info that interests me will be of any help to you.
2. Hacks, resources, how-tos, where tos and other info that will help YOU travel.
3. How the travel went. Skip this "one more blog on personal travelogue made public"

### Context
Mountains fascinate me. Few months back, I came across this classification of travelers based on whether s/he likes mountains or beaches. Respectively s/he is called a mountain person or a beach person. I guess, I am a mountain person. Nepal has mountains 8P. I like like mountains. Motivation found; Level 99999. The offer was made by an awesome human being called Killer Queen a.k.a KQ (not disclosing her name for the fear of death 8P)

It was a 10 day long trip in <abbr title="This is when travel in Nepal starts getting tractions">February of 2019</abbr>. The duration makes us tourists, not travellers or backpackers, although we mostly backpacked on shoestrings. Although we were tourists, but our style of travel was definitely backpacking.

##### Style of travel
I work (for the moment) with Tata Research as a Design Researcher. Freshly joined. More or less it is the first job. Similar is the case with KQ. We gathered most of the information from reddit, lonely planet, expedia, trip advisor. We prefer hopping from place to place; deciding where to spend time. I called it high entropy travel. We prefer to walk because shoestring was tight (8P), it is healthy, and you get exposed to lot many stimulants than in a taxi. I guess this makes us cultural tourists. But we absorb whatever comes in the way; making and validating our hypothesis, observing closely (-_-) comparing with our previous experiences, asking around.

### Planning
Planning of the trip went up and down, back and forth, right and wrong. From google sheets to discussions. In the end, after enough data was collected about Nepal, KQ came up with interesting thing. We _geographically_ plotted places in Nepal, and timeline we had. See image

This gave me an idea to develop a tool that *aids* travel planning. Any planner needs two things: Places that can be visited and the time one has. The travel time will be pre-fed (or can be set as per the transport mode selected) and the places can be dragged and dropped on to the timeline. The tool then calculates the journeys needed, stays needed, and probably determines the fatigue level. Such a tool can be optimize the travel experience. More on that later.

Accommodations were done using hostelworld. Again a fun idea by KQ: Each one of us books half of the places to stay. And no one discloses them to each other. It would have been fun, but the time and situations were short. Flights were booked beforehand using conventional tools; only if I had come across this awesome tool to get the cheapest flight. Kudos to Senseable cities lab for [the greatescape](https://greatescape.co/)! Here is our 10-day Nepal trip plan:

![](planv.jpg)

### Hacks for the impatient

##### Travel in Nepal
###### Traveling in cities:
Most cities have taxis, auto-rickshaws for private transport. They charge you decided rates, but there are no objective ways of measuring those charges (like a meter or a rate card). Rates are pre-decided between you and the driver. One can haggle responsibly to a reasonable rate. But, what's the fun in that? Fun resides in knowing how local people travel, how do they navigate, what do they eat/drink, what do they talk about, what do they do while traveling, whom do they travel with, and so forth. And most big cities like Kathmandu, Pokhara have sorted public transport. I am sure other cities would have as well, but the point being, all it takes is time to figure out and some enthusiasm.

> Mind==blown when I found out about [tootle app](https://play.google.com/store/apps/details?id=com.three60.cabioclient&hl=en). It is a ride sharing app for bike commuters. I did not get a chance to use it, but if you are traveling solo or are in a hurry in Kathmandu, it can prove much useful. I do not know its operational coverage, so check before using it in Pokhara or Lumbini or elsewhere. Thanks to my senior Pranisha for this! :)

###### Traveling between cities:
Inter-city transport is also sorted in Nepal. Again, one can always choose taxi to travel, but I will mostly detail out public transport. In this case you have options as well. Buses run from around various city centres. But the boarding points depend on where you want to go. So ask around or to concierges of your hostel/hotel. Local people are super helpful and they point you cheapest and their preferred solution to your problem (which is usually the best). Buses are of several kinds:
+ Tourist buses-luxury comes with a price. They specially cater to tourists and are run by very few companies. Jagdamba travels is one of them. You can reserve the seats by call or in person at their local offices or hop-in if there's any space left.
+ Semi luxurious buses-They are luxurious buses from yonder-times. Also run by similar companies and seat reservations can be made at local offices or by phone calls.
+ Public buses-lesser leg room, okayish public (KQ would differ with me on this), and jumpy seats. Well, imagine for yourself.

> It was interesting to find out that there exists online bus booking portal for Nepal. This could sort out lots of hassle for tourists: what are the bus timings, where to reach, what is the usual price, are they looting? . I guess it is not publicized enough. Reader, bless me by using [bussewa.com](http://bussewa.com). <abbr title="I know it is not even https">Sniff sniff 8(</abbr>.

##### Money and Transactions
_This section is valid only if you are from India or you have Indian currency_
The documentation about using cash and other transactions is pretty ambiguous on the web. There is no set method known. It can cause a bit of foo-farawing before travel. Atleast it caused a bit to me.

Cash Transactions:
In Nepal, locals use the term 'IC' in their day to day life for 'Indian currency'. It is Indian and NOT 'International currency'. I had assumed otherwise for the first few hours in Nepal. And they use 'Nepalese' (in short for 'Nepalese currency') for, well 'Nepalese currency'. As of Feb. 2019, following holds verified:
+ There is a constant/fixed exchange rate between IC and NC (1 IC = 1.6 NC). Or 1 INR = 1.6 NPR.
+ Legally, ICs are accepted <abbr title="No use of going to an exchange, you might get even lesser rates (around 1.5 instead of 1.6)">*everywhere*</abbr>, BUT, as per receiver's willingness. So ask the receiver if s/he will receive ICs for that transaction.
+ Only old 100 ICs are accepted. Any other denominations are not accepted.
+ Any newer currency notes (ones released after demonetisation in India, 2016) are not accepted.

Cashless transactions:
If you have any Indian bank debit card, you are good to go. There are several cards with conditions as:
"Not for payment in Foreign Exchange in Nepal and Bhutan."
Such cards can still be used at <abbr title="ATMs in Nepal are NOT owned by banks. Several companies own ATM spaces and machines">ATMs </abbr> and POS devices. Of course, the transaction and conversion charges will be applied. MYTH==BUSTED!!!

If you have an SBI card, you are blessed with no conversion charges in all SBI Nepal ATMS. Cheers! You get a flat conversion rate. Them bilateral ties ! :)

##### Immigration
_For Indian Passport holders only_

We traveled by air, and yes, you have to clear immigration procedures. Also, you get those entry/exit stamps on your passports. I am not sure how immigration by land works. Some say you can just walk-in to Nepal with your Voter ID. Have not tried.

##### Accomodation:
Like described earlier, we backpacked the trip (because high entropy). Hostels were our choice. You get to know new people, have interesting activities and usually, hostels are more friendlier than hotels. Homestays are another great option, but, hostels are abundant in Nepal. Preferred websites are the popular ones like [hostelworld.com](http://hostelworld.com/) and [booking.com](https://www.booking.com/).

Zostels have pretty maintained standard of amenities, quality of services and charges. If you've taken enough risks on your trip and want to assured of a good night's sleep, Zostels are the safest option in India and Nepal. Experiment with local hostels at will.

##### Communication
There are several network operators available in Nepal. N-Cell, Smart Cell, Nepal telecom and Namaste to name a few popular ones or the ones whose billboards were seen. They are easily available upon a copy of your passport and a photograph. Folks and Nepali friends had said N-Cell has good coverage in Nepal; and we stand by it. We seldom faced a 'No service situation'.




### The Log

Nepal Travelogue

For family, friends, and interested people, here is my travelogue! *[Unable to view properly?](http://www.dee-am.in/site/htmlPages/nepalLog.html)*. I would prefer watching it on full screen through [the link](http://www.dee-am.in/site/htmlPages/nepalLog.html) on a computer/tablet.

<iframe src="https://uploads.knightlab.com/storymapjs/41740e183fbca748a4c6b0da543da78c/nepal/index.html" frameborder="0" width="100%" height="500"></iframe>


I think using words to describe feelings is an ages old idea. It becomes cheesy and words start becoming similar to other blogs because there's only one language and one thesaurus to it. So, here's a small visualisation of my feelings during Bungee jumping. *Alternatively*, see <a href="http://www.dee-am.in/site/htmlPages/bungee.html" target="_blank">here</a> in a larger viewport. 

<iframe src="https://cdn.knightlab.com/libs/storyline/latest/embed/index.html?dataURL=https%3A%2F%2Fdocs.google.com%2Fspreadsheets%2Fd%2F1Z6hRhPm7Tc7wqJmDCV9rZRqmI5VKpIwOEYWSmvsBk1A%2Fedit%23gid%3D0&amp;dataYCol=scariness&amp;dataXCol=timemilliseconds&amp;dataDateFormat=%25Y&amp;chartDateFormat=%25Y&amp;chartYLabel=Distance%20from%20life&amp;sliderCardTitleCol=whathappened&amp;sliderCardTextCol=whatitfeltlike" style="width:100%;height:650px;" marginwidth="0" marginheight="0" vspace="0" hspace="0" frameborder="0"></iframe>

❤ | Nepal 

PS: Timeline is made using StoryMapJS, and the Bungee visualisation is made using StorylineJS by knightlab folks. KnightLab has interesting story telling JS frameworks. Do [check them out](https://knightlab.northwestern.edu/).
