---
title: 'Nepal with an interesting pal'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
published: false
taxonomy:
    category:
        - blog
    tag:
        - music
        - kerala
        - audioRepo
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

Mostly a record of moments, some hacks, some resources, and some must-do things in Nepal. Tooltips in this blogs are like tiny mental notes that I made. If I were you, I would not ignore the tooltips of this blog. 
I am aware most people are short of time in hearing out travel experiences, forget about reading them. THe blog does not intend to recommend places, but provide various info that I did not find on the web before visiting Nepal. Hence, I have structured the blog as following:
1. A bit of context, what kind of travel do I prefer and such contextual info. This can help you choose if things and info that interests me will be of any help to you. 
2. hacks, resources, how-tos, where tos and other info that will help YOU travel. 
3. How the travel went. Skip this "one more blog on personal travelogue made public"
4. General observations about Nepal. 

===

### The context
The trip was 10 day long, stimulated by an inspiring person called Surbhi. We went from 1st Feb to 10th Feb. 
. Motivation to do the trip
. vacation type
. Budget
. Nature of us
. Indians

### The planning
. Done by surbhi, I piggybacked. 
. The route versus map visualisation tool. Design idea for a tool that aids in such planning
. Flight tickets (when to book, resources: Senseable labs)
. Accomodation booking (idea mention)
. The final plan 

Travel:
Inter-city travel
Commute (tootle)

Cash:
SBI Nepal
Nepal banned cards

Food: 
Specific to places

Passport:
Immigrated stamp



### Day 1 in Nepal
. Kathmandu > Taxi (amount, haggling) > Exchange of currencies (not possible, 100 rupees) > SIM Card > Book for bungee jumping (#jump smart people, choosing package), food (local joints). 

. About airport

##### Bus to Pokhara
Haggling, scenario, fare (480). Commute by taxi, bus. Lack of visibility of online booking (when I found one)

### Day 2 Pokhara
Commute: Buses, taxi
. Accomodation: Zostel Pokhara (location, facilities)
. Pokhara: Lake side versus Damside, Fewa, surrounded by the majestic lines of mountains (names of mountains)

Pokhara:
Davis falls, Gupteshwor Mahadev caves (Continuation of Davis falls, entry is interesting, end of the fall is the most interesting. (Hike)
Shanti stupa (Hike, runners high, through village, villagers are extremely helpful, might want to wear shorts/runners pants instead of jeans for trail running) (Photo). Three can reached by foot within half a day. Beware, you are kicked off of stupa by 6:30. Star gazing. Food Hopping 
Sarangkot Early morning, Tibetan monastery (story versions > buddha > story > Mandalas), Begnas lake (Serenity, zone out, buses stop going after 6:30 or so, taxis to the rescue, Surbhi's quote about there is a place that is off beat). Food hopping again. Different part of the city explored (metro-ish, catering to wealthy/high budget cafes). Many restaurants here have live music/dance. It was like asian (if not international) food/booze festival going on forever.

Lumbini:
Travel: bus from Pokhara to Bhairahwa, types of buses. Travel to Lumbini from Bhairahawa via an auto/taxi. Roads are terrible, if you have back problem or such, taxi it. Welcome to almost India. Where mongolians speak bhojpuri/UP style hindi :) Felt home. 
 Regret: Should have taken a night bus probably. 
Worth visiting for a day or more. Lots of monasteries. Lots. It is like a cultural festival going on forever. Onwards to Kathmandu

Kathmandu:
First long stay at Kathmandu
Patan: 
Patan Museum, guides, audio guides, funded by Austria. Takes abour 5-6 hours. Guides not good, but worth exploring. Museum is an archaeological storehouse of artifacts as old as xxxx BC. Arts on/of metals, stones are preserved and explained well in the audio guide and even in the placards. THe audio guide covers more than just the museum, it covers the Patan durbar square as well. The cafe next to museum called cafe du temple is nice place to eat apart from the cafe inside. Patan durbar square is surrounded by markets. around 1 km from the square is the bus stop that can take you to various towards kathmandu. 

Stupa swayambhunath:
Walk, steps, monkeys, monkey temple, view of Kathmandu if skies are clear. 

Thamel:
Thamel at night is bae. It looks like everything is lit. It is hyper-westernised. From super stores to cake shops to cafes. Highly westernised. 

Happy Birthday Surbhi! :)

Bungeeeeeee
The last resort: shop in thamel, also book online. Bus from thamel. Dont sleep off on the journey, it is extremely beautiful. Feelings are more expressed in this graph than by words. (plan to make it reactive WIP please). Stay at the resort is tooooo peaceful, starry, and confortable. It felt nice after travel of these many days. They serve Nepalese and some continental dishes. there is no way to get out of the place on your own except local buses which are very very few. Did not test it, but the area is kind of isolated. If possible, look for tents numbered 13, 15, 14. They are closer to river Bagmati, you can hear her roars day in and out. Get a skyviewer app, star gaze if you're in luck. 
About last resort: 
Sustainable economy/living, bridge. Nice people, good design, management. 
Things to look for: How an entire business is costructed around a bridge. Bungee > Resort > Restaurant > Cafe > Photos (where they shoot you by default, show you and then you choose on whether to buy or not. I think most people will buy. It is better than asking whether you wish to get your video or not?)
It was a lovely stay

Bhaktapur:
Cultural things lots of them. Durbar square is huge than Patan. Wood carvings open museum. Peacock store: interesting antique store, baghchal games. Put your head into one of the tiny stores to xplore different cultural artefacts. They are huge and stuffed from inside. Lucky to be present in Bhaktapur on Vasant panchami. Two hindu rituals, a movie shoot. 

Souvenirs: 
Cultural souvenirs: Bhaktapur
Hip trendy souvenirs: Pokhara (cult road), Thamel
Religious souvenirs: Lumbini
Natural souvenirs: Mountains> Go on a trek, collect himalayan salt/fossils (dream at the moment)

Opinions:
1. Sweet/safe/smart people, helpful
2. Culture is inherently happy. Even in cities, general folks are not tensed or loaded. They are happy
3. One can safely choose Zostels over other backpacker hostels. Because you know what to expect. However, we found that people at zostels are less accomodating to locals than in other backpacker hostels. 
4. Nepal is divided into 3 parts: Pahad, Terai, and Himal. Land locked. Economy is based on agriculture, tourism, and earnings from foreign nationals.

I hope the post cards reach us, till then, images. 



<iframe src="http://timemapper.okfnlabs.org/malaydhamelia/timeline?embed=1" frameborder="0" style="border: none;" width="150%" height="780px;" align="center"></iframe>

