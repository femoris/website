---
title: 'Reads'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - list

twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: ''

publish_date: 7/30/2018 11:05 pm
published: true

---

A list of books that serves as a to-do owing to my forgetfulness, and as a palette of taste and brand of literature for seeking more.

===

+ Thinking fast and slow
+ Nudge
+ Wealth of Nations
+ What I talk about, when I talk about running
+ Uses of argument
+ Tractacus-logico-philosophicus
+ The Discworld series
+ Ecritis
+ At the existential cafe
+ Embassy Town
+ Haroon and the stories
+ Three cups of tea
+ One hundred years of solitude
+ Batman and the philosophy
+ Love in the time of cholera
+ God of small things
+ Society of the spectacle
+ Seeing things as they are
+ Imagination- a study in the history of ideas
+ Creativity and its cultivation
+ Judgement under uncertainty
+ Thought
+ Designing design
+ 70 Essays on design
+ Critical Theory
+ A history of civilisations
+ Homo Sapiens, Deus
+ The great transformation
+ Time enough for love
+ Behind a Billion Screens: What Television Tells Us About Modern India
+ Animal farm
+ We are like that only
+ Beyond good and evil
+ The power of will
+ Sense of style
+ On writing well
+ What is this thing called Science?
+ La Nausea
+ Sophie's World
+ Enlightenment Now


The mentioned books are not for a particular purpose. Hence, they are haphazard and probably chaotic. I intend to make this page a bit more informative with classifications, critical analysis of the books, and their authors, easter eggs, etc. I'll be adding more books once I recollect them. 

We all are ready to accept stimulus. Stimulate me with your suggestions of interesting, radical books and authors. [Mail me](mailto:malaydhameliareads@gmail.com)
