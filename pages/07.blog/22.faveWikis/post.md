---
title: 'Favourite wikis'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - list
        - resource
        - articles
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: ''

publish_date: 7/30/2018 11:05 pm
published: true
---

A list containing interesting [wiki](https://en.wikipedia.org/wiki/Wiki)s. 

===

## Favourite wikis
For the recently found out love of wikis of different domains and on different subjects, I will update them in this page

+ [ErgoPedia](http://www.ergopedia.ca/)
+ [Designwiki](http://deseng.ryerson.ca/dokuwiki/start)
+ [WikiIndex](https://wikiindex.org/Welcome)-a wiki about wikis 

Lists are dynamic. Think you have interesting aggregators/articles or the sources ? Send an [email](mailto:malaydhameliareads@gmail.com)