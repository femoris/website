---
title: 'Measuring suspense in a storyline'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - research idea
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: ''

published: true
---

A sporadic idea generated while binge-watching Jessica Jones, when my half-asleep mind started asking a lot of questions after every montage. Contains ideas for methodologies in information economics, story telling. This seed can also be stimulated individually in day to day conversations and while designing courses. It contains some interesting reads about information economics and wild references to blow your weekend off !

===

Information economics deals with [suspense and surprise](https://www.nytimes.com/2015/04/26/opinion/sunday/the-economics-of-suspense.html). People have done a wicked amount of work to optimise, model, and generate suspense and surprise in stories(novels, video games, events, films, etc.) [using machines](https://link.springer.com/chapter/10.1007/978-3-540-89454-4_21) or by a life long penance. However, a quick web and scholar search yielded that there are very few reliable techniques for measuring suspense and surprise. Some attempts to measure suspense in experiments related to storytelling include asking questions at a section/chapter end. Such questions majorly expect likert based responses and minorly expect brief descriptions about experiences from the viewers. 


I think this kind of experiment design has some inherent flaw in it. Imagine you are told to answer some kind of question periodically while you are immersed in the story. One, the immersion of the story decreases. Two, the results you get are biased since viewers start focusing on finding suspense, comparing it with previous experience, and grading them WHILE watching/reading/playing. A more nuanced method would be to record the brain signals or muscle tension or chemical changes in the body. But, who likes invasion ?


Can we approach this measurement, by taking inspiration from think-aloud methods ? The user mentally asks questions like 'Why was the pen-drive shown?' 'What are the possible moves ?', 'Why was this umbrella shown at this time?', 'Maybe she will use this against him in this manner...' and so forth. It would be interesting to identify the characteristics of such questions and mapping them against levels (weights) of suspense. Some characteristics of questions that come to mind directly are:
1. Hypotheses
2. Probable moves of protagonist/antagonist
3. Anticipation of events
4. Anticipation of usage of certain situations and/or in certain situations
Such characteristics can be found out by performing a thematic analysis, content analysis (or specifically discourse analysis with self).

  ___
 (o,o)