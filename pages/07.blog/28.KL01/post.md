---
title: 'KL-01'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - music
        - kerala
        - audioRepo
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: 'Photography is prohibited. And so is vandalism'

published: true
---

To stimulating conversations with some of the TCS peeps!
The spell of boredom ended on the 1st Saturday of September. 

===

+ Song: [Fish Rock](https://soundcloud.com/tvkappa/fish-rock-thaikkudam-bridge). Find more malyali rock music at the end of the post, as suggested by Sooriyan and Gokul.  


_The suitor of believers_
![](krishnaJayanti.JPG)

+ Stimulus: Studies on happiness, measurement of happiness.

Abhijith, Dinoj, and Avinash. You have a lovely home. 

##### Montage #1

How to design systems for sadness ? For making you feel owned ? Or worst, for making you feel insignificant ? There could be interfaces for curbing one's way of expression or one's way of life. This is beyond, nudging. 

The experience of first day at CLC _(Nobody knows what CLC stands for ? or was even bothered to ask. So much for curiosity! So much for research!)_ was gut clenching. Using computers and internet, and entering in the system (of mechatrons) is sadenning. The interface is designed such that you feel owned. It is not about blocking content, there is more. 

Log in using a password, then one more password, then one more password. Passwords are cool. But what lies behind those passwords is what matters. You take in courses about security, about how to be a responsible employee. Those passwords unlock timesheets which are needed to be filled in. I agree, one is accountable for one's own work, but this is beyong nudging. The accountability in employees is not created or induced by positive re-inforcement. This something more cruel than coercion. Since this type of design for behaviour change is de-humanising*. The primary reason I feel is that the rules are designed for carpet bombing. 

Making a general rule that is constricting enough so that there are no chances of failure. Its like locking a child inside of a house, for the fear of him making some mistake or some mistake to happen to him. This is essentially a culture of fear. But, who are scared in this case? The organisations. What are they scared of ? Almost anything. And fear, like any other human emotions, is contagious. It is transmitted to the organisations stakeholders. Every stakeholder. With money, the fear also trickles down. If the organisation is couragious, the stakeholders will be couragious. To think, to do. 

If the organisation is bold, the stakeholders will be bolder. Why do you think the students of IITs have inherent rigour and self-respect ? Because the organisation has balls. 

And so much for the quality of work! Like I implied, that there is a reciprocating nature of qualities. What the company strives for, it has to be reflected in philosophy of the system, for stakeholders are absorbers of this philosophy. And they only reciprocate what they are given. Stakeholders only give that much, and in that manner in proportion to what they think the company is worth. 

Folks asked about salaries, promotion, etc. Nobody asked for access to research papers. They asked for leaves, working hours, and policies for work from home. Nobody discussed the scenario in which, if they want to work/research extra during odd hours. Nobody asked if they wanted to work immediately after a midnight idea. I am not blaming them. I am asking, would it make the organisation better, if the system was such that, folks are inclined otherwise ?

Organisation does not want to do shit. 
"What to do if I do not find happiness in the place I am located ?" "If I am not able to give my best because my mind is not in place ?"
> Organisation will kick you off ! 

Without considering alternatives ? The system is not designed for alternatives ?

It was a lovely evening when I started to walk towards my hostel. Sun cheered me up, with his hands full of rays. Dandellions were dancing on the beats of trade winds. There was a beautiful shot of dandellions (or some other kind of translucent tropical flower, I dont know. For I did not get the courage to enquire that because I knew I would be tempted to click a photo. Not allowed. Aversion. Curiosity off). Is it okay to stay in an environment where this kind of curiosity is curbed through design? Is it okay to stay in an enviroment where appreciation of beauty holds back an individual ? Will the prolonged stay make me insensitive towards things ?

*Irony is established since I am going to work in Behavioural sciences division. 

### Malyali indie songs:
1. [Nada Nada - Avial](https://www.youtube.com/watch?v=lPsz2raYePI)
2. [Ellarum Chollanu - Amrutham Gamaya](https://www.youtube.com/watch?v=wiEAUMwVAjM)
3. [One - Thaikuddam Bridge](https://www.youtube.com/watch?v=XSZHjIWX6qY)
