---
title: 'Some thoughts on modern music of India'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - music
        - audioRepo
        - list
        - discovery
        - sawaal
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: ''

publish_date: 8/2/2018 12:06 pm
published: true
---

 

===

With a stimulating [read from the Commonweal magazine](https://www.commonwealmagazine.org/remnant-and-restless-crowd) detailing out the suicide plan of the west, and with a micro-worry about the effect of rainlessness in the central and south-central India, [this playlist](https://www.youtube.com/watch?v=fgjjJOZkXbQ&index=1&list=PLG0f76gvWfh810C24WDXBbwITTD4r0nlZ) of A.R Rahman gave a sense of warmth. Reminiscing about his older discography and single pieces like _Jiya jale_, _Tu hi re_ _Thok de killi_, _Chinnamma Chilakamma_, I could see a pattern of experimentation. Radical in his youth, his older songs still resonate with me. A similar pattern I would like to observe in Amit Trivedi, since I am ageing up with his music. 

[plugin:youtube](https://www.youtube.com/watch?v=6oxkWWfiVHs)

A glimpse of Amit Trivedi's work:
[plugin:youtube](https://www.youtube.com/watch?v=1EtI0KjXfdk)

Also, listening to these guys music triggers another thought. Prof. Girish once told us about Indian Modernity and how one of the remains of Indian modernity was in the cultures of Tawaefs in Lucknow. Most other composed pieces find it tough to strike a balance their western influence and the indian classical influence. They incline towards either. Indo-western music is not modern. Such music can be steps to discover the new wave of Indian music, but they do not exemplify modern India's music. I am no musician, and have limited knowledge about music, but, I find their music more Indian. I find music from these composers to hit that sweet spot where one can safely ask— can we say these music pieces as artefacts of Indian Modernity in music ?