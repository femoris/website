---
title: 'Process of separation--a how to guide'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - procedure
        - work
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: ''

published: true
---

A general sense of the process to exit from my ex-employer. Strictly administrative!

===

Once you have received the good news from university of your choice or finally decided to jump up to some next level in your career or you have finally realised after two years that are pretty much bored, follow these steps to exit from the company. 

The procedure is highly conditional. So here are the conditions under which the procedure applies:
1. There is a resignation
2. There is a shortfall from the notice period


### Step 0: Informal intimation (avoiding nuclear devastation)
Inform your lead, RA lead, supervisor, manager about the possibility of you resigning with whatever reason. This gives her/him enough time to find a replacement or manage things

### Step 1: Formal intimation (no nuclear devastation)
Once you are sure of your decision, you have planned, and you have a final date of your stay, drop an official email requesting the company to accept your resignation. The recepients usually are: Supervisor, lead and your RA head; with local HRs in CC. This is the day from which your 90-days notice period start.

Here, you are persuaded, lured, etc etc by various superiors. Because this is the last chance they are getting to keep you from going. They are ejected from the process of separation once they approve of your resignation request.  

### Step 2: Pestering HR
Pester HR weekly to initiate the process. Ask her/him to send you the tracker* on your email. Usually they are lazed out and inactive; only activated once there are two weeks left of your release date. 
Send back the filled tracker to ensure smooth separation process. Separation cell takes in from this stage, after you submit the filled tracker. Meanwhile, make sure, you are giving a good KT, documenting your work, sending and sharing it to relevant superiors. 

Here, you are persuaded, given options by local HR. They act like a bridge between you and the separation cell. You interact directly with separation cell. 

_*Tracker is a table containing all your details. This is used to track your process of separation. HR sends you an empty table via email which you have to fill and send back_

At this point, you might be asked to fill out exit interview in ultimatix. 
### Step 3: Pay up 
If there is a shortfall of notice period days, you are asked to pay amount proportional to the shortfall days. Now, to pay this, you receive account details from separation cell. *Make sure you have an active company email id, else they will have to send it to your personal email id*. And one very well knows that company is a one way fort for information. Emails do not reach to personal email ids. 

_Ultimatix usually kicks you out of company system on the eve of your release, so it should be fine. However, in my case, it kicked me out two days prior to my release date. So, there were some hustles here and there._ 

### Step 4: Clearance form
Pay the amount and you shall receive the offline clearance form. There's one online clearnace form as well, but for that, the process of separation needs to start from approximately a month before your release date (seldom happens!). 

Take signatures from building HR (against submission of your ID Card), IS Team (against submission of your machine and devices), and company VISA officer/local HR (if you have a VISA issued for and from the company).I do not know what happens to that VISA since I did not have one from the company; however, I think they might apply for the cancellation/withdrawal of the issued VISA if it is still valid at the time of release. Also, this might take time, so, earlier the better. 

Once the form is filled, one can choose to send it to SP to separation cell via internal courier or simply scan that paper, SAVE IT IN PDF, and email it to separation cell. 

This marks the end of process from your side. You do not have to anything except submitting the ID card to security desk and wait for the initial release letter.   

Cheers !
Good Luck to you whenever you leave ! 8P