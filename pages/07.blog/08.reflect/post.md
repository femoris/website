---
title: 'Self-reflection- the rabbithole'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - reflections
        - realisations
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: ''

publish_date: 08/23/2017 05:50 pm
published: false
---

This post documents experiences, effects, and influences of design education, of IDC on me. A bittersweet symphony as it was, some myths about the design education are debunked, some hypothesis are formed, some ideas are generated. It quotes some people at IDC, and how they influenced me in my thought processes. This post includes a pre-test/post-test analysis of my thoughts. 

===

#### A prequel
I entered IDC as a believer of randomness, unstructured in thoughts, one thing I was certain that a good education is one that allows you to self-criticise and makes you capable to look objectively at the world and yourself. 