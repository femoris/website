---
title: 'Love Death and Robots'
headline: ''
media_order: ''
published: true
taxonomy:
    category:
        - blog
    tag:
        - processingJS
        - interactive
        
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

A processing JS Sketch to generate different images inspired from love, death and robots icons. Plus some praises of Fincher, Tim Miller, and LD+R. 

This post is also heavy on tool tips with random but personally interesting trivia. I hope tool tips are not ignored 8). 

===

Recently I was introduced to this nearly cult anthology produced by [David Fincher](https://en.wikipedia.org/wiki/David_Fincher) _(producer/director of some of my favourites -- Mindhunter, House of Cards, Fight Club, Alien, The Girl with the Dragon Tattoo, The Case of Benjamin Button)_ called [Love, Death, and Robots](https://en.wikipedia.org/wiki/Love,_Death_%26_Robots) (LD+R, in short). The anthology is a potpourri of animated stories of varying length and varying animation styles by various artists. Thanks to Vajra for introducing this to me!

One could think on most of them <abbr title="You know your Latin 8P">ad infinitum</abbr>, but I was blown away by the concept of <abbr title="It is just 6 minutes, KQ 8P. No frets though">Zima Blue</abbr> and the radical cinematography/animation of <abbr title="My second most favorite cinematographical shots after The Matrix Trilogy">The Witness</abbr>. This Saturday was the day of fangirling a bit; to Fincher, yet another awesome <abbr title="First one being Frank Miller">Miller</abbr> and to LD+R! Let the jam <abbr title="see what I did there">be-gin</abbr>!

#####Idea: 
To have a new whatsapp DP every day for a good number of days (I know, it is cheeky. But fangirling is irrational.)

#####Inspiration: 
Left side of the image which can be used as a whatsapp DP
![](inspi.jpg)

#####Primal mind: 
Make one everyday(inkscape), export as jpg, save it on some cloud (google photos), download it to phone, have a DP

#####Bored mind on RedBull: 
Make a processing sketch (it's been long since you have used processing)

#####Help: 
Cheers to [Michael Chernyak & Ofer Ariel](https://uxuihero.com/love-death-robots-free-fan-iconfont/) who took the toil of making each and every icon from the anthology and providing them in 8 <abbr title="png, svg, ai, eps, pdf, png combined (sprite processing ready), psd, and last but not the least, MIND == BLOWN for WOFF AND TTF !!! ❤">different formats</abbr>

Here's an open source+quick+sketchy processing/processingJS sketch for your perusal and use-al 8P. Click on reload button to generate various square images. If your browser allows pop-ups, **it will also download** a square image to your file system. Use it however, whenever, wherever, but at your own discretion 8).

_If unable to view properly, view in full viewport [here](http://www.dee-am.in/site/htmlPages/ldr/ldr.html)_

<iframe src="http://www.dee-am.in/site/htmlPages/ldr/ldr.html" width="100%" height="600"></iframe>

**PS:** Since this is licensed under CC, feel free to use the images rendered through this sketch and the sketch itself in any manner, however you wish. Some example usages of the rendered images is t-shirts, mugs, social media profile pics, etc. 