---
title: 'Work culture be like 30 Rock'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - favourites
        - TV(Humor)
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: 'Work culture be like 30 Rock'

publish_date: 7/30/2018 11:05 pm
published: false
---

|Season/Episode | Episode name | Comments |
|---------------|--------------|----------|
|1/10|	The Rural Juror| |
|1/11|	The head and the hair| |
|1/12|	Black tie| |
|1/13|	The C word|[Where is my runt ?]|
|2/3|	The collection|	The encounter of Jack and his private investigator|
|4/9|	Klaus and Greta| Imma add one girl to my entourage|
|4/13|	Anna Howard Show day| Liz Lemon getting tripsy|


Also, I think [Rachel Dratch](https://en.wikipedia.org/wiki/Rachel_Dratch) is as cool as Tina