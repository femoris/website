---
title: Blog
content:
    items: '@self.children'
    order:
        by: date
        dir: desc
    limit: 40
    pagination: true
    
---

