---
published: false
title: 'Interesting people of the web'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - list
        - resource
        - articles
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: 'Interesting people of the web'

publish_date: 7/30/2018 11:05 pm
published: false
---

NOT people with interesting websites (because, websites are conduits) or interesting people with websites (well, it would be worth knowing such people yourself. It is fate dictated.)

===
+ [Moiz Syed](http://moiz.ca/) is a designer and a coder. He deals with information design, data journalism, and designs for activism. 
