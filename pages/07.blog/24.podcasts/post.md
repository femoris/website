---
title: 'List of interesting podcasts'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - list
        - resource
        - articles
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: ''

publish_date: 7/30/2018 11:05 pm
published: true
---

I find podcasts very revolutionary. A novel approach in transmitting information. If they are clean, they require only a part of your attention and yet, do a fair job in conveying the message.

===

Podcast Series 

+ Ted Radio Hour
+ Planet Money (Thanks to [Astrid Bin](http://www.astridbin.com/))
+ Freakonomics
+ Revisionist history (Malcolm Gladwell)
+ Maed in India (An interview podcast dedicated to indie music scene in India)
