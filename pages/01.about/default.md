---
title: About
media_order: localrpo.jpeg
body_classes: 'title-center title-h1h2'
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: true
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true

person:
  contact:
    email: malay.n.dhamelia@gmail.com

  portrait:
    url: me_.png
    alt: Malay Dhamelia

download:
  url: resume.pdf
  alt: Resume

taxonomy:
  category:
    - function
  tag:
    - me
---

> This is an all-text website with very few images, but it is [lynx](https://en.wikipedia.org/wiki/Lynx_(web_browser)) friendly. 8)

*updated: September 30, 2019*


**My Portfolio can be found [here](http://www.dee-am.in/portfolio)**


I am a first year PhD Scholar at IDC School of Design, Indian Institute of Technology Bombay, India. Prime focus of my PhD is towards understanding the nature of fun obtained through play. For now, I aim at developing ontologies that will help me develop theories related to fun obtained through play. For now, I am looking at multiple dimensions of fun-- sociological, cultural, transactional. 


I have previously worked at the prestigous Tata Research, Design, and Development Center (TRDDC), Pune. As a part of my job, I designed services, conducted field studies in various contexts, developed computational tools to aid in design research. 


I have graduated from IDC School of Design, IIT Bombay as Interaction Designer. Just like in my previous <abbr title="B.Tech, Electrical Engineering">education</abbr> I found more things about my interests apart from the design programme. I am more interested in asking questions, and finding answers. Although I came to IDC to solve one question---"Why am I making, what I am making?", I found the joy of finding answers to unanswered questions. After Interaction design course, I found the trade of knowledge fascinating. I do not think this section is going to stay updated as since life is organic, and full of surprises. However, I'll try to keep this website for representation of myself in the best manner as possible.
</br>


> "A human being should be able to change a diaper, plan an invasion, butcher a hog, conn a ship, design a building, write a sonnet, balance accounts, build a wall, set a bone, comfort the dying, take orders, give orders, cooperate, act alone, solve equations, analyze a new problem, pitch manure, program a computer, cook a tasty meal, fight efficiently, die gallantly. Specialization is for insects."
> — Robert Heinlein, Time Enough for Love


I am a _believer_ of this quote, trying to be a _follower_ as well. So, I try to keep my information platter as varied as possible, at the same time, not being a _information-hippy_. I majorly live in information hunter-gatherer reality, and try to keep my experiences as diverse as possible. I believe, having varied experiences not only allow us to survive, but have fun in life as well. There is some virtue associated with knowing and doing things diversely. I try to be virtuous in that regard.


### My Interests + work lies in following domains
 + Programmable arts (generative arts, algorithmic arts, and so forth) 
 + Games Design, Digital games
 + Creative technologies
 + Interactive web tech
 + Tangible interactions
 + Design Research
 + Social psychology
 + Behaviour economics
 + Economics
 + Design methods
 + Music
 + Philosophy

</br>
</br>
 CV updated on 30th September, 2019: [CV](http://www.dee-am.in/site/about/MalayResume.pdf)